<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package start
 */

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Leaders in Continuous Improvement | </title>
  <link rel='stylesheet' id='roboto_one-css'  href='http://fonts.googleapis.com/css?family=Roboto%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.2.12' type='text/css' media='all' />
<link rel='stylesheet' id='raleway_two-css'  href='http://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.2.12' type='text/css' media='all' />
<link rel='stylesheet' id='raleway_three-css'  href='http://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.2.12' type='text/css' media='all' />
  
	<link rel="shortcut icon" href="http://wp.beepo.com.ph/wp-content/uploads/2015/05/favicon.png" type="image/x-icon">
	<link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/style.css??ver=4.2.12" type="text/css" media="all">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" media="all">
  <link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/css/preset1.css" type="text/css" media="all">
  <link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/css/shortcodes.css" type="text/css" media="all">
  <script type="text/javascript" src="<?php bloginfo("template_url"); ?>/js/jquery.min.js"></script>


<?php wp_head(); ?>	
</head> 


<body <?php body_class(); ?>>
	
<div id="navigation" class="navbar navbar-default">
	<div class="container">
		 <div class="navbar-header">
           <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand scroll" href="http://wp.beepo.com.ph/">
              <img src="http://wp.beepo.com.ph/wp-content/uploads/2017/02/LeadCI-Logo.jpg" title="">
            </a>
         </div>

         <div class="navbar-collapse collapse">
            <ul id="menu-main-menu" class="nav navbar-nav">
               <?php
                 $menus  =  wp_get_nav_menu_items('Main Menu');
                 foreach($menus as $menu){
                     echo "<li><a href='#".$menu->post_name."'>".$menu->post_title."</a></li>";
                 }
               ?>
            </ul>
            <script type="text/javascript">
            $(document).ready(function(){
                   $('#menu-main-menu li').first().addClass( "active" );

                   $('#menu-main-menu li').click(function(){
                       $('#menu-main-menu > li').removeClass("active");
                       $(this).addClass("active");
                   });

            });
            </script>											
         </div>
	</div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $(window).scroll(function() {
         $('#our-company').css({"margin-top":"90px"});
      if ($(this).scrollTop() > 20) { // this refers to window
         $('div#navigation').addClass("navbar-fixed-top");
         $('#our-company').css({"margin-top":"0px"});
      }else{
         $('div#navigation').addClass("navbar-fixed-top");
         $('#our-company').css({"margin-top":"90px"});
      }
    });
  });
</script>